USE ModernWays;
SET SQL_SAFE_UPDATES = 0; -- Je moet dit zetten als je iets wilt uitvoeren maar vergeet het niet uit te schakelen
UPDATE Huisdieren SET Leeftijd=9 
WHERE Baasje= 'Christiane' AND soort ='hond' OR Baasje='Bert' AND soort='kat';
-- Bij set moet invoerwaarde komen en Where is de specifieke adressering WAAR je de vrerandering wilt invoeren.
-- Stel je wilt de ene andere leeftijde en de ene een andere dan moet je dit dat apart invoeren. dus je kan niet 2 waardes invoeren bij SET;
SET SQL_SAFE_UPDATES = 1;