USE ModernWays;
SET SQL_SAFE_UPDATES=0;
DELETE  FROM Liedjes; -- De syntax van DELETE lijkt erg op die van SELECT, maar in plaats van bepaalde rijen te tonen, zal MySQL ze gewoon wissen.
-- Je kan ook geen specifieke kolommen wissen, dus je schrijft DELETE FROM Boeken en niet DELETE * FROM Boeken
SET SQL_SAFE_UPDATES=1;