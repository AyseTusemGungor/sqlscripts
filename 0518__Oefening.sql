USE ModernWays;
-- I am using alter table because i want to add a column to a table that already exists
-- Third, MySQL allows you to add the new column as the first column of the table by specifying the FIRST keyword. It also allows you to add the new column after 
-- an existing column using the AFTER
alter table  Huisdieren add column Geluid VARCHAR (20);  -- je maakt tabel
Set SQL_SAFE_UPDATES= 0; -- zet beveiliging uit
update Huisdieren SET Geluid = 'WAF!' where Soort= 'hond' ;-- schrijf bij kolom geluidenwaf indien soort een hond is
update Huisdieren SET Geluid = 'miyavv!'  where Soort= 'kat' ;-- nogmaals update huisdieren typen is belangrijk
Set SQL_SAFE_UPDATES = 1;

